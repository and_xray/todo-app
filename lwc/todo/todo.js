import { LightningElement, track } from 'lwc';
import { updateRecord } from 'lightning/uiRecordApi';
import { createRecord } from 'lightning/uiRecordApi';
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getLists from '@salesforce/apex/TODOListController.getLists';
import getItems from '@salesforce/apex/TODOItemController.getItems';
import getItemsCount from '@salesforce/apex/TODOItemController.getItemsCount';
import TODOITEM_OBJECT from '@salesforce/schema/TODO_Item__c';
import ID_FIELD from '@salesforce/schema/TODO_Item__c.Id';
import NAME_FIELD from '@salesforce/schema/TODO_Item__c.Name';
import DUE_DATE_FIELD from '@salesforce/schema/TODO_Item__c.Due_Date__c';
import COMPLETED_FIELD from '@salesforce/schema/TODO_Item__c.Completed__c';
import LIST_ID_FIELD from '@salesforce/schema/TODO_Item__c.TODO_List__c';

export default class Todo extends LightningElement {

    lists;
    items;
    @track error;
    currentListId;
    today = new Date();
    newItemName = '';
    newItemDate = '';
    @track satrtDate;
    endDate;
    @track dateInterval = 'All';
    totalRecordsNumber;
    pageNumber = 1;
    @track isAll = true;
    editedItemId = null;

    get intervalOptions() {
        return [
            { label: 'All', value: 'All' },
            { label: 'Day', value: 'Day' },
            { label: 'Week', value: 'Week' },
            { label: 'Month', value: 'Month' },
        ];
    }

    connectedCallback() {
        this.getListValues();
        this.satrtDate = (this.satrtDate) ? this.satrtDate : this.today.toJSON().slice(0, 10);
    }

    handleListChange(event) {
        this.currentListId = event.detail;
        this.getListValues();
        this.getTotalRecordsNumber();
    }

    handleListRefresh(event) {
        this.getListValues();
    }

    getListValues() {
        getLists()
            .then(result => {
                this.lists = result;
                if (this.currentListId) {
                    this.getItemValues();
                    this.getTotalRecordsNumber();
                } else {
                    this.currentListId = this.lists[0].Id;
                    this.getItemValues();
                    this.getTotalRecordsNumber();
                }
            })
            .catch(error => {
                this.showToast('Error', 'Error getting lists from server', 'error');
                console.log(error.body.message);
            });
    }

    getItemValues() {
        getItems({
            listID: this.currentListId,
            dateStart: this.satrtDate,
            dateInterval: this.dateInterval,
            pageNumber: this.pageNumber
        })
            .then(result => {
                this.items = result;
            })
            .catch(error => {
                this.showToast('Error', 'Error getting items from server', 'error');
                console.log(error.body.message);
            });
    }

    getTotalRecordsNumber() {
        this.totalRecordsNumber = 0;
        getItemsCount({
            listID: this.currentListId,
            dateStart: this.satrtDate,
            dateInterval: this.dateInterval
        })
            .then(result => {
                this.totalRecordsNumber = result;
            })
            .catch(error => {
                this.showToast('Error', 'Error getting item count from server', 'error');
                console.log(error.body.message);
            });
    }

    get groupedItemArray() {
        if (this.items) {

            let groupedDataMap = new Map();
            this.items.forEach(item => {
                if (!item.Due_Date__c) {
                    let noDate = 'No due date';
                    if (groupedDataMap.has(noDate)) {
                        groupedDataMap.get(noDate).items.push(item);
                    } else {
                        let newItem = {};
                        newItem.Due_Date__c = noDate;
                        newItem.items = [item];
                        groupedDataMap.set(noDate, newItem);
                    }
                } else {
                    if (groupedDataMap.has(item.Due_Date__c)) {
                        groupedDataMap.get(item.Due_Date__c).items.push(item);
                    } else {
                        let newItem = {};
                        newItem.Due_Date__c = item.Due_Date__c;
                        newItem.items = [item];
                        groupedDataMap.set(item.Due_Date__c, newItem);
                    }
                }
            });
            return groupedDataMap.values();
        } else {
            return [];
        }
    }



    showToast(toastTitle, toastMessage, toastVariant) {
        dispatchEvent(
            new ShowToastEvent({
                title: toastTitle,
                message: toastMessage,
                variant: toastVariant
            })
        );
    }

    handleNameChange(event) {
        this.newItemName = event.target.value;
    }

    handleDateChange(event) {
        this.newItemDate = event.target.value;
    }

    createItem() {
        if (!this.currentListId) {
            this.showToast('Error', 'Please select a list!', 'error');
        } else if (!this.newItemName) {
            this.showToast('Error', 'Please enter item name!', 'error');
        } else {
            let fields = {};
            fields[NAME_FIELD.fieldApiName] = this.newItemName;
            fields[LIST_ID_FIELD.fieldApiName] = this.currentListId;
            fields[DUE_DATE_FIELD.fieldApiName] = this.newItemDate;
            let recordInput = { apiName: TODOITEM_OBJECT.objectApiName, fields };
            createRecord(recordInput)
                .then((newItem) => {
                    this.showToast('Success', 'Item added', 'success');
                    this.newItemName = '';
                    this.newItemDate = '';
                    this.getListValues();
                    this.getItemValues();
                    this.getTotalRecordsNumber();
                })
                .catch((error) => {
                    this.showToast('Error', 'Error creating item', 'error');
                    console.log(error.body.message);
                });
        }
    }

    handleDelete(event) {
        deleteRecord(event.currentTarget.dataset.id)
            .then(() => {
                this.showToast('Success', 'Item deleted', 'success');
                this.getListValues();
                this.getItemValues();
                this.getTotalRecordsNumber();
            })
            .catch(error => {
                this.showToast('Error', 'Error deleting item', 'error');
                console.log(error.body.message);
            });
    }

    handleCompletedChange(event) {
        let fields = {};
        fields[ID_FIELD.fieldApiName] = event.currentTarget.dataset.id;
        fields[COMPLETED_FIELD.fieldApiName] = event.target.checked;
        let recordInput = { fields };
        updateRecord(recordInput)
            .then(() => {
                this.showToast('Success', 'Item updated', 'success');
                this.getListValues();
                this.getItemValues();
                this.getTotalRecordsNumber();
            })
            .catch((error) => {
                this.showToast('Error', 'Error updating item', 'error');
                console.log(error.body.message);
            });
    }

    handleSatertDateChange(event) {
        if (event.target.value) {
            this.satrtDate = event.target.value;
            this.getItemValues();
            this.getTotalRecordsNumber();
        } else {
            this.showToast('Error', 'Please select date', 'error');
        }
    }

    handleDateIntervalChange(event) {
        this.dateInterval = event.target.value;
        this.isAll = (this.dateInterval === 'All') ? true : false;
        this.getItemValues();
        this.getTotalRecordsNumber();
    }

    handlePageChange(event) {
        this.pageNumber = event.detail;
        this.getItemValues();
    }

    handleEdit(event) {
        this.editedItemId = event.currentTarget.dataset.id;
        this.newItemName = event.currentTarget.dataset.name;
        this.newItemDate = event.currentTarget.dataset.duedate;
        this.template.querySelector("section").classList.remove("slds-hide");
        this.template.querySelector("div.modalBackdrops").classList.remove("slds-hide");
    }

    handleClose() {
        this.newItemName = '';
        this.newItemDate = null;
        this.editedItemId = null;
        this.template.querySelector("section").classList.add("slds-hide");
        this.template.querySelector("div.modalBackdrops").classList.add("slds-hide");
    }

    handleSaveEdit(event) {
        if (!this.newItemName) {
            this.showToast('Error', 'Please enter item name!', 'error');
        } else {
            let fields = {};
            fields[ID_FIELD.fieldApiName] = this.editedItemId;
            fields[NAME_FIELD.fieldApiName] = this.newItemName;
            fields[DUE_DATE_FIELD.fieldApiName] = this.newItemDate;
            let recordInput = { fields };
            this.handleClose();
            updateRecord(recordInput)
                .then(() => {
                    this.showToast('Success', 'Item is updated', 'success');
                    this.getItemValues();
                })
                .catch((error) => {
                    this.showToast('Error', 'Error updating item', 'error');
                    console.log(error.body.message);
                });
        }
    }

}
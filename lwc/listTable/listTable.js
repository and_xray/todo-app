import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { createRecord } from 'lightning/uiRecordApi';
import { deleteRecord } from 'lightning/uiRecordApi';
import { updateRecord } from 'lightning/uiRecordApi';
import TODOLIST_OBJECT from '@salesforce/schema/TODO_List__c';
import NAME_FIELD from '@salesforce/schema/TODO_List__c.Name';
import ID_FIELD from '@salesforce/schema/TODO_List__c.Id';

export default class ListTable extends LightningElement {

    @api refresh;
    @api todoLists;
    selectedListId;
    @track newListName = '';
    @track originalMessage;
    @track isDialogVisible = false;
    editedListId = null;

    columns = [' ', 'ToDo List', 'Status', 'Items', ''];

    renderedCallback() {
        this.selectRow();
    }

    get lists() {
        if (this.todoLists) {
            if (!this.selectedListId) {
                this.selectedListId = this.todoLists[0].Id;
            }
        }
        return this.todoLists;
    }

    showToast(toastTitle, toastMessage, toastVariant) {
        dispatchEvent(
            new ShowToastEvent({
                title: toastTitle,
                message: toastMessage,
                variant: toastVariant
            })
        );
    }

    getFirstRowID() {
        if (this.lists) {
            return this.lists[0].Id;
        }
    }

    handleNameChange(event) {
        this.newListName = event.target.value;
    }

    createList() {
        if (!this.newListName) {
            this.showToast('Error', 'Please enter list name!', 'error');
        } else {
            let fields = {};
            fields[NAME_FIELD.fieldApiName] = this.newListName;
            let recordInput = { apiName: TODOLIST_OBJECT.objectApiName, fields };
            createRecord(recordInput)
                .then((newList) => {
                    this.showToast('Success', 'List added', 'success');
                    this.newListName = '';
                    this.sendListRefresh();
                })
                .catch((error) => {
                    this.showToast('Error', 'Error creating list', 'error');
                    console.log(error.body.message);
                });
        }
    }

    handleDelete(event) {
        if (event.target) {
            if (event.target.name === 'openConfirmation') {
                this.originalMessage = event.currentTarget.dataset.id;
                this.isDialogVisible = true;
            } else if (event.target.name === 'confirmModal') {
                if (event.detail !== 1) {
                    if (event.detail.status === 'confirm') {
                        if (this.lists[0].Id === event.detail.originalMessage) {
                            this.selectedListId = this.lists[1].Id;
                        } else {
                            this.selectedListId = this.lists[0].Id;
                        }
                        deleteRecord(event.detail.originalMessage)
                            .then(() => {
                                this.showToast('Success', 'List deleted', 'success');
                                this.sendListChange();
                            })
                            .catch(error => {
                                this.showToast('Error', 'Error deleting list', 'error');
                            });
                    }
                }
                this.isDialogVisible = false;
                this.selectRow();
            }
        }
    }

    handleEdit(event) {
        this.editedListId = event.currentTarget.dataset.id;
        this.newListName = event.currentTarget.dataset.name;
        this.template.querySelector("section").classList.remove("slds-hide");
        this.template.querySelector("div.modalBackdrops").classList.remove("slds-hide");
    }

    handleSaveEdit(event) {
        if (!this.newListName) {
            this.showToast('Error', 'Please enter list name!', 'error');
        } else {
            let fields = {};
            fields[ID_FIELD.fieldApiName] = this.editedListId;
            fields[NAME_FIELD.fieldApiName] = this.newListName;
            let recordInput = { fields };
            this.handleClose();
            updateRecord(recordInput)
                .then(() => {
                    this.showToast('Success', 'List is updated', 'success');
                    this.sendListRefresh();
                })
                .catch((error) => {
                    this.showToast('Error', 'Error updating list', 'error');
                    console.log(error.body.message);
                });
        }
    }


    handleRowSelect(event) {
        this.selectedListId = event.currentTarget.dataset.recordid;
        this.sendListChange();
    }

    sendListChange() {
        const event = new CustomEvent('listchange', { detail: this.selectedListId });
        this.dispatchEvent(event);
    }

    sendListRefresh() {
        const event = new CustomEvent('listrefresh');
        this.dispatchEvent(event);
    }

    selectRow() {
        if (this.selectedListId) {
            let toSelect = '[data-recordid="' + this.selectedListId + '"]'
            let rowRadio = this.template.querySelector(toSelect);
            rowRadio.checked = true;
        }
    }

    handleClose() {
        this.newListName = '';
        this.editedListId = null;
        this.template.querySelector("section").classList.add("slds-hide");
        this.template.querySelector("div.modalBackdrops").classList.add("slds-hide");
    }

}
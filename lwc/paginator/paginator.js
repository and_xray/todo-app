import { LightningElement, track, api } from 'lwc';

export default class Paginator extends LightningElement {

    @api recordsTotal;
    perPage = 10;
    @track page = 1;
    @track pages = [];
    @track numberOfPages;    

    connectedCallback() {
        this.getPages();
    }

    getPages() {
        this.numberOfPages = Math.ceil(this.recordsTotal / this.perPage);
        for (let index = 1; index <= this.numberOfPages; index++) {
            this.pages.push(index);
        }
    }

    renderedCallback() {
        this.renderButtons();
    }

    renderButtons = () => {
        const BLUE = '#1B5297';
        const WHITE = 'white';
        this.template.querySelectorAll('.pnum').forEach((but) => {
            but.style.backgroundColor = this.page === parseInt(but.dataset.id, 10) ? BLUE : WHITE;
            but.style.color = this.page === parseInt(but.dataset.id, 10) ? WHITE : BLUE;
        });
    }

    get isFirst() {
        return this.page === 1;
    }

    get isLast() {
        return this.page === this.numberOfPages;
    }

    nextButtonHandler = () => {
        ++this.page;
        this.sendPageChange()
    }

    prevButtonHandler = () => {
        --this.page;
        this.sendPageChange()
    }

    onPageClick = (e) => {
        this.page = parseInt(e.target.dataset.id, 10);
        this.sendPageChange()
    }

    sendPageChange() {
        const event = new CustomEvent('pagechange', { detail: this.page });
        this.dispatchEvent(event);
    }
}
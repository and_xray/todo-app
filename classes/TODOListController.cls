public with sharing class TODOListController {

	@AuraEnabled
	public static List<TODO_List__c> getLists() {
		PermissionChecker.exceptionIfNotAccessible(TODO_List__c.getSObjectType());
		return [SELECT Name, Status__c, Number_of_Items__c FROM TODO_List__c ORDER BY Name LIMIT : TODOControllerHelper.LIMIT_50K];
	}
}

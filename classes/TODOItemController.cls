public with sharing class TODOItemController {

	private static final Integer RECORDS_PER_PAGE = 10;

	@AuraEnabled
	public static List<TODO_Item__c> getItems(Id listID, Date dateStart, String dateInterval, Integer pageNumber) {
		PermissionChecker.exceptionIfNotAccessible(TODO_Item__c.getSObjectType());
		TODOControllerHelper.checkParams(listID, dateStart, dateInterval);
		TODOControllerHelper.checkPageNumber(pageNumber);
		Date dateEnd = TODOControllerHelper.setDateEnd(dateStart, dateInterval);
		dateStart = TODOControllerHelper.setDateStart(dateStart, dateInterval);
		Integer pageOffset = (pageNumber - 1) * RECORDS_PER_PAGE; 
		return [
			SELECT Name, Completed__c, Due_Date__c, TODO_List__c
			FROM TODO_Item__c
			WHERE TODO_List__c = :listID 
					AND ((Due_Date__c >= :dateStart 
							AND Due_Date__c <= :dateEnd) 
						OR Due_Date__c = null)
			ORDER BY Due_Date__c
			LIMIT :RECORDS_PER_PAGE
			OFFSET :pageOffset
		];
	}

	@AuraEnabled
	public static Integer getItemsCount(Id listID, Date dateStart, String dateInterval) {
	PermissionChecker.exceptionIfNotAccessible(TODO_Item__c.getSObjectType());
	TODOControllerHelper.checkParams(listID, dateStart, dateInterval);
	Date dateEnd = TODOControllerHelper.setDateEnd(dateStart, dateInterval);
	dateStart = TODOControllerHelper.setDateStart(dateStart, dateInterval);
	return [
		SELECT count()
		FROM TODO_Item__c
		WHERE TODO_List__c = :listID 
				AND ((Due_Date__c >= :dateStart 
						AND Due_Date__c <= :dateEnd)
					OR Due_Date__c = null)
	];
	}
}
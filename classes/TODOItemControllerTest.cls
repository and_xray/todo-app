@IsTest
public with sharing class TODOItemControllerTest {


	@testSetup
	private static void setup() {
		PermissionChecker.exceptionIfNotCreateable(TODO_List__c.getSObjectType());
		PermissionChecker.exceptionIfNotAccessible(TODO_List__c.getSObjectType());
		PermissionChecker.exceptionIfNotCreateable(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotAccessible(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotUpdateable(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotDeletable(TODO_Item__c.getSObjectType());
	}

	@isTest
	private static void testGetItemsPageNumberParameter() {
		Integer numberOfLists = 1;
		Integer numberOfItems = 15;
		Date dateStart = Date.today();
		String dateInterval = TODOControllerHelper.ALL;
		Integer pageNumber = 1;

		List<TODO_List__c> testLists = TestDataFactory.createLists(numberOfLists);
		insert testLists;
		insert TestDataFactory.createItemsForLists(testLists, numberOfItems);
		Id listId = testLists[0].Id;
		Test.startTest();

		List<TODO_Item__c> actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
		System.assertEquals(10, actualItems.size());

		pageNumber = 2;
		actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
		System.assertEquals(5, actualItems.size());

		pageNumber = 3;
		actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
		System.assertEquals(0, actualItems.size());

		Test.stopTest();
	}

	@isTest
	private static void testGetItemsDateIntervalParameter() {
		Integer numberOfLists = 1;
		Integer numberOfItems = 38;

		List<TODO_List__c> testLists = TestDataFactory.createLists(numberOfLists);
		insert testLists;
		List<TODO_Item__c> testItems = TestDataFactory.createItemsForLists(testLists, numberOfItems);
		for (Integer i = 0; i < numberOfItems; i++) {
			testItems[i].Due_Date__c = Date.today().addDays(i - 2);
		}
		insert testItems;

		Test.startTest();
		Id listId = testLists[0].Id;
		Date dateStart = Date.today();
		List<TODO_Item__c> actualItems;

		String dateInterval = TODOControllerHelper.DAY;
		Integer pageNumber = 0;
		Integer actualTotalItemsNumber = 0;
		do {
			pageNumber++;
			actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
			actualTotalItemsNumber = actualTotalItemsNumber + actualItems.size();
			for (TODO_Item__c actualItem : actualItems) {
				System.assertEquals(dateStart, actualItem.Due_Date__c);
			}
		} while (actualItems.size() != 0);
		System.assertEquals(1, pageNumber - 1);
		System.assertEquals(1, actualTotalItemsNumber);

		dateInterval = TODOControllerHelper.WEEK;
		pageNumber = 0;
		actualTotalItemsNumber = 0;
		do {
			pageNumber++;
			actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
			actualTotalItemsNumber = actualTotalItemsNumber + actualItems.size();
			for (TODO_Item__c actualItem : actualItems) {
				System.assert (dateStart <= actualItem.Due_Date__c);
				System.assert (dateStart.addDays(6) >= actualItem.Due_Date__c);
			}
		} while (actualItems.size() != 0);
		System.assertEquals(1, pageNumber - 1);
		System.assertEquals(7, actualTotalItemsNumber);

		dateInterval = TODOControllerHelper.MONTH;
		pageNumber = 0;
		actualTotalItemsNumber = 0;
		do {
			pageNumber++;
			actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
			actualTotalItemsNumber = actualTotalItemsNumber + actualItems.size();
			for (TODO_Item__c actualItem : actualItems) {
				System.assert (dateStart <= actualItem.Due_Date__c);
				System.assert (dateStart.addDays(30) >= actualItem.Due_Date__c);
			}
		} while (actualItems.size() != 0);
		System.assertEquals(4, pageNumber - 1);
		System.assertEquals(31, actualTotalItemsNumber);

		dateInterval = TODOControllerHelper.ALL;
		pageNumber = 0;
		actualTotalItemsNumber = 0;
		do {
			pageNumber++;
			actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
			actualTotalItemsNumber = actualTotalItemsNumber + actualItems.size();
		} while (actualItems.size() != 0);
		System.assertEquals(numberOfItems, actualTotalItemsNumber);
		System.assertEquals(4, pageNumber - 1);

		Test.stopTest();
	}

	@isTest
	private static void TestGetItemsCount() {
		Integer numberOfLists = 1;
		Integer numberOfItems = 38;

		List<TODO_List__c> testLists = TestDataFactory.createLists(numberOfLists);
		insert testLists;
		List<TODO_Item__c> testItems = TestDataFactory.createItemsForLists(testLists, numberOfItems);
		for (Integer i = 0; i < numberOfItems; i++) {
			testItems[i].Due_Date__c = Date.today().addDays(i - 2);
		}
		insert testItems;

		Test.startTest();
		Id listId = testLists[0].Id;
		Date dateStart = Date.today();

		String dateInterval = TODOControllerHelper.DAY;
		Integer actualItemsCount = TODOItemController.getItemsCount(listId, dateStart, dateInterval);
		System.assertEquals(1, actualItemsCount);

		dateInterval = TODOControllerHelper.WEEK;
		actualItemsCount = TODOItemController.getItemsCount(listId, dateStart, dateInterval);
		System.assertEquals(7, actualItemsCount);

		dateInterval = TODOControllerHelper.MONTH;
		actualItemsCount = TODOItemController.getItemsCount(listId, dateStart, dateInterval);
		System.assertEquals(31, actualItemsCount);

		dateInterval = TODOControllerHelper.ALL;
		actualItemsCount = TODOItemController.getItemsCount(listId, dateStart, dateInterval);
		System.assertEquals(numberOfItems, actualItemsCount);

		Test.stopTest();
	}

	@isTest
	private static void TestParamExceptions() {
		Integer numberOfLists = 1;
		Integer numberOfItems = 1;

		List<TODO_List__c> testLists = TestDataFactory.createLists(numberOfLists);
		insert testLists;
		List<TODO_Item__c> testItems = TestDataFactory.createItemsForLists(testLists, numberOfItems);
		insert testItems;

		Test.startTest();

		Id listId = null;
		Date dateStart = Date.today();
		String dateInterval = TODOControllerHelper.DAY;
		Integer pageNumber = 1;
		try {
			List<TODO_Item__c> actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
		} catch (Exception e) {
			System.assert (e.getMessage().contains(TODOControllerHelper.INCORRECT_LIST_ID));
		}

		listId = testLists[0].Id;
		dateStart = null;
		dateInterval = TODOControllerHelper.DAY;
		pageNumber = 1;
		try {
			List<TODO_Item__c> actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
		} catch (Exception e) {
			System.assert (e.getMessage().contains(TODOControllerHelper.INCORRECT_DATE_START));
		}

		listId = testLists[0].Id;
		dateStart = Date.today();
		dateInterval = 'Incorrect';
		pageNumber = 1;
		try {
			List<TODO_Item__c> actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
		} catch (Exception e) {
			System.assert (e.getMessage().contains(TODOControllerHelper.INCORRECT_DATE_INTERVAL));
		}

		listId = testLists[0].Id;
		dateStart = Date.today();
		dateInterval = TODOControllerHelper.DAY;
		pageNumber = 0;
		try {
			List<TODO_Item__c> actualItems = TODOItemController.getItems(listId, dateStart, dateInterval, pageNumber);
		} catch (Exception e) {
			System.assert (e.getMessage().contains(TODOControllerHelper.INCORRECT_PAGE_NUMBER));
		}

		Test.stopTest();
	}
}

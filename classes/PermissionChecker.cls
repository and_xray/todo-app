public class PermissionChecker {

	private static final String NO_PERMISSION = 'Current user does not have permission to ';
	private static final String TO_ACCESS = 'access ';
	private static final String TO_UPDATE = 'update ';
	private static final String TO_DELETE = 'delete ';
	private static final String TO_CREATE = 'create ';

	public static void exceptionIfNotAccessible(Schema.SObjectType Obj){
		if (!Obj.getDescribe().isAccessible()) {
			throw new HasNoAccessException(NO_PERMISSION + TO_ACCESS + Obj);
		}
	}

	public static void exceptionIfNotUpdateable(Schema.SObjectType Obj){
		if (!Obj.getDescribe().isUpdateable()) {
			throw new HasNoAccessException(NO_PERMISSION + TO_UPDATE + Obj);
		}
	}

	public static void exceptionIfNotDeletable(Schema.SObjectType Obj){
		if (!Obj.getDescribe().isDeletable()) {
			throw new HasNoAccessException(NO_PERMISSION + TO_DELETE + Obj);
		}
	}

	public static void exceptionIfNotCreateable(Schema.SObjectType Obj){
		if (!Obj.getDescribe().isCreateable()) {
			throw new HasNoAccessException(NO_PERMISSION + TO_CREATE + Obj);
		}
	}
}
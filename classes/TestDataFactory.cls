public with sharing class TestDataFactory {

	private static final String INCORRECT_NUM_LISTS = 'Incorrect numLists, should be more than 0';
	private static final String INCORRECT_NUM_ITEMS_PER_LIST = 'Incorrect numItemsPerList, should be more than 0';

	public static List<TODO_List__c> createLists(Integer numLists) {
		if ((numLists < 1) || (numLists == null)) {
			throw new IncorrectParameterException(INCORRECT_NUM_LISTS);
		}
		List<TODO_List__c> lists = new List<TODO_List__c>();
		for(Integer i = 0; i < numLists; i++) {
			TODO_List__c testList = new TODO_List__c(Name = 'TestList_' + i);
			lists.add(testList);
		}
		return lists;
	}

	public static List<TODO_Item__c> createItemsForLists(List<TODO_List__c> lists, Integer numItemsPerList) {
		if ((numItemsPerList < 1) || (numItemsPerList == null)) {
			throw new IncorrectParameterException(INCORRECT_NUM_ITEMS_PER_LIST);
		} else{
			List<TODO_Item__c> items = new List<TODO_Item__c>();
			for (Integer j = 0; j < lists.size(); j++) {
				for (Integer k = 0; k < numItemsPerList; k++) {
					items.add(new TODO_Item__c(
							  Name = 'TestItem_' + k + '_for_' + lists[j].Name,
							  TODO_List__c = lists[j].Id
							  ));
				}
			}
			return items;
		}
	}
}

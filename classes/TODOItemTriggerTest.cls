@isTest
public class TODOItemTriggerTest {

	@testSetup
	private static void setup() {
		PermissionChecker.exceptionIfNotCreateable(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotAccessible(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotUpdateable(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotDeletable(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotCreateable(TODO_List__c.getSObjectType());
		PermissionChecker.exceptionIfNotAccessible(TODO_List__c.getSObjectType());
		PermissionChecker.exceptionIfNotUpdateable(TODO_List__c.getSObjectType());
	}

	@isTest
	private static void testMultipleListChangeStatus() {
		Test.startTest();

		List<TODO_List__c> lists = TestDataFactory.createLists(2);
		insert lists;
		lists = [SELECT Name, Status__c FROM TODO_List__c];
		for (TODO_List__c testList :lists) {
			System.assertEquals(testList.Status__c, TODOControllerHelper.EMPTY);
		}

		List<TODO_Item__c> items = TestDataFactory.createItemsForLists(lists, 3);
		insert items;
		lists = [SELECT Name, Status__c FROM TODO_List__c];
		for (TODO_List__c testList :lists) {
			System.assertEquals(testList.Status__c, TODOControllerHelper.IN_PROGRESS);
		}

		for (TODO_Item__c item :items) {
			item.Completed__c = true;
		}
		update items;
		lists = [SELECT Name, Status__c FROM TODO_List__c];
		for (TODO_List__c testList :lists) {
			System.assertEquals(testList.Status__c, TODOControllerHelper.COMPLETED);
		}

		delete items;
		lists = [SELECT Name, Status__c FROM TODO_List__c];
		for (TODO_List__c testList :lists) {
			System.assertEquals(testList.Status__c, TODOControllerHelper.EMPTY);
		}

		undelete items;
		lists = [SELECT Name, Status__c FROM TODO_List__c];
		for (TODO_List__c testList :lists) {
			System.assertEquals(testList.Status__c, TODOControllerHelper.COMPLETED);
		}

		Test.stopTest();
	}

	@isTest
	private static void testListWithDifferentItemsChangesStatus() {
		Test.startTest();

		List<TODO_List__c> testList = TestDataFactory.createLists(1);
		insert testList;
		testList = [SELECT Name, Status__c FROM TODO_List__c LIMIT 1];
		System.assertEquals(testList[0].Status__c, TODOControllerHelper.EMPTY);


		List<TODO_Item__c> items = TestDataFactory.createItemsForLists(testList, 2);
		insert items;
		testList = [SELECT Name, Status__c FROM TODO_List__c LIMIT 1];
		System.assertEquals(testList[0].Status__c, TODOControllerHelper.IN_PROGRESS);

		items[0].Completed__c = true;
		update items[0];
		testList = [SELECT Name, Status__c FROM TODO_List__c LIMIT 1];
		System.assertEquals(testList[0].Status__c, TODOControllerHelper.IN_PROGRESS);

		delete items[1];
		testList = [SELECT Name, Status__c FROM TODO_List__c LIMIT 1];
		System.assertEquals(testList[0].Status__c, TODOControllerHelper.COMPLETED);

		undelete items[1];
		testList = [SELECT Name, Status__c FROM TODO_List__c LIMIT 1];
		System.assertEquals(testList[0].Status__c, TODOControllerHelper.IN_PROGRESS);

		items[1].Completed__c = true;
		update items[1];
		testList = [SELECT Name, Status__c FROM TODO_List__c LIMIT 1];
		System.assertEquals(testList[0].Status__c, TODOControllerHelper.COMPLETED);

		delete items;
		testList = [SELECT Name, Status__c FROM TODO_List__c LIMIT 1];
		System.assertEquals(testList[0].Status__c, TODOControllerHelper.EMPTY);

		Test.stopTest();
	}
}
public with sharing class TODOControllerHelper {

	public static final String EMPTY = 'Empty';
	public static final String COMPLETED = 'Completed';
	public static final String IN_PROGRESS = 'In Progress';
	public static final String ERROR_GENERAL = 'Error occured. Please try again later';
	public static final Integer LIMIT_50K = 50000;
	public static final String ALL = 'All';
	public static final String DAY = 'Day';
	public static final String WEEK = 'Week';
	public static final String MONTH = 'Month';
	public static final String INCORRECT_LIST_ID = 'TODOItemController: Incorrect ListId paramener. Can not be null.';
	public static final String INCORRECT_DATE_START = 'TODOItemController: Incorrect dateStart paramener. Can not be null.';
	public static final String INCORRECT_DATE_INTERVAL = 'TODOItemController: Incorrect dateInteval paramener. Must be All, Week, Month or Day.';
	public static final String INCORRECT_PAGE_NUMBER = 'TODOItemController: Incorrect pageNumber paramener. Must be 1 or more.';

	public static Date setDateEnd(Date dateStart, String dateInterval){
		Date dateEnd = null;
		if (dateInterval == TODOControllerHelper.DAY) {
			dateEnd = dateStart;
		} else if (dateInterval == TODOControllerHelper.WEEK) {
			dateEnd = dateStart.addDays(6);
		} else if (dateInterval == TODOControllerHelper.MONTH) {
			dateEnd = dateStart.addDays(30);
		}
		return dateEnd;
	}

	public static Date setDateStart(Date dateStart, String dateInterval){
		return dateInterval == TODOControllerHelper.ALL ? null : dateStart;
	}

	public static void checkParams(Id listID, Date dateStart, String dateInterval){
		if (listID == null) {
			throw new IncorrectParameterException(TODOControllerHelper.INCORRECT_LIST_ID);
		}

		if (dateStart == null) {
			throw new IncorrectParameterException(TODOControllerHelper.INCORRECT_DATE_START);
		}

		if ((dateInterval != TODOControllerHelper.ALL)
		    && (dateInterval != TODOControllerHelper.DAY)
		    && (dateInterval != TODOControllerHelper.WEEK)
		    && (dateInterval != TODOControllerHelper.MONTH)) {
			throw new IncorrectParameterException(TODOControllerHelper.INCORRECT_DATE_INTERVAL);
		}
	}

	public static void checkPageNumber(Integer pageNumber){
		if ((pageNumber == null) || (pageNumber < 1)) {
			throw new IncorrectParameterException(TODOControllerHelper.INCORRECT_PAGE_NUMBER);
		}
	}
}

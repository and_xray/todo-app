@isTest
public with sharing class TODOListControllerTest {

	@isTest
	private static void testTODOListController() {
		PermissionChecker.exceptionIfNotCreateable(TODO_List__c.getSObjectType());
		PermissionChecker.exceptionIfNotAccessible(TODO_List__c.getSObjectType());
		Integer numberOfLists = 5;
		Test.startTest();
		List<TODO_List__c> actualLists = TestDataFactory.createLists(numberOfLists);
		insert actualLists;
		List<TODO_List__c> expectedLists = TODOListController.getLists();
		System.assertEquals(numberOfLists, expectedLists.size());
		for (TODO_List__c expectedList :expectedLists) {
			System.assertEquals(TODOControllerHelper.EMPTY, expectedList.Status__c);
			System.assertEquals(0, expectedList.Number_of_Items__c);
		}
		Test.stopTest();
	}
}
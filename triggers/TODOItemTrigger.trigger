trigger TODOItemTrigger on TODO_Item__c (after insert, after update, after delete, after undelete) {

	if (Trigger.isUpdate && Trigger.isAfter) {
		setListStatus(Trigger.new);
	}

	if (Trigger.isInsert && Trigger.isAfter) {
		setListStatus(Trigger.new);
	}

	if (Trigger.isDelete && Trigger.isAfter) {
		setListStatus(Trigger.old);
	}

	if (Trigger.isUndelete && Trigger.isAfter) {
		setListStatus(Trigger.new);
	}

	private static void setListStatus(List<TODO_Item__c> items){
		PermissionChecker.exceptionIfNotAccessible(TODO_Item__c.getSObjectType());
		PermissionChecker.exceptionIfNotUpdateable(TODO_List__c.getSObjectType());

		Set<Id> listIDs = new Set<Id>();
		for (TODO_Item__c item: items) {
			listIDs.add(item.TODO_List__c);
		}

		List<TODO_List__c> listsForUpdate = new List<TODO_List__c>();
		for (Id listID : listIDs) {

			List<AggregateResult> groupedLists = [
				SELECT Completed__c, TODO_List__c, TODO_List__r.Status__c
				FROM TODO_Item__c
				WHERE TODO_List__c = :listID
				GROUP BY TODO_List__c, Completed__c, TODO_List__r.Status__c
				LIMIT :TODOControllerHelper.LIMIT_50K
			];
			String newStatus;
			if (groupedLists.isEmpty()) {
				newStatus = TODOControllerHelper.EMPTY;
				listsForUpdate.add(new TODO_List__c(Id = listID, Status__c = newStatus));
			} else {
				newStatus = TODOControllerHelper.COMPLETED;
				for (AggregateResult groupedList :groupedLists) {
					if (groupedList.get('Completed__c') == false) {
						newStatus = TODOControllerHelper.IN_PROGRESS;
					}
				}
				if (newStatus != groupedLists[0].get('Status__c')) {
					listsForUpdate.add(new TODO_List__c(Id = listID, Status__c = newStatus));
				}
			}
		}
		update listsForUpdate;
	}
}
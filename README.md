## Simple ToDo app.

#### Ligtning Web Componenets, APEX controllers and APEX triggers were used.

App includes task lists with ToDo tasks.

List/task can be added, edited and deleted. Also, each task could be marked as complete/incomplete. Modal windows are used for edit functionality. All notifications are displayed using ShowToastEvent.

Task can have due date. All tasks are grouped by due date in the list. Tasks without due date are displayed at the top of the list.

Paginator allows navigating to different pages if the list contains more than 10 tasks.

Filter by time intervals: all, day, week and month. It is possible to select starting day of the time interval.

Each task list has status and tasks counter. Status changed by APEX trigger.
    If list does not contain tasks, list status is Empty.
    If list contains incomplete tasks, list status is In Progress.
    If list contains only complete tasks, list status is Completed.

APEX tests are included.

![ToDo App](/uploads/9c992a3ca6af5db3e30ad8257c1b97d0/Скриншот_2020-06-05_09.53.16.png)

![Editing task modal window](/uploads/f95fdaf08276d7d9f3b144a86b2600a9/Скриншот_2020-06-05_09.52.58.png)

![Delete confirmation modal window](/uploads/79abb36b7cdf8c100e982c12c637e375/Скриншот_2020-06-05_09.55.03.png)

![Error notification toast message](/uploads/19ee85294bdbaa7188b9c42b7c0493af/Скриншот_2020-06-05_09.53.33.png)